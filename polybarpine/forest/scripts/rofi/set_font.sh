#!/usr/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
CONFIGFILE="../../config.ini"

CONFIGFILEFONT=$(grep ^font-0 $CONFIGFILE)
FONT=$(echo $CONFIGFILEFONT | cut -d "\"" -f2  | cut -d ":" -f1)
SIZE=$(echo $CONFIGFILEFONT | cut -d "=" -f3  | cut -d ";" -f1)
echo "setting all fonts to: $FONT $SIZE"
sed -i "/font/s/.*/\tfont:\t\"${FONT} ${SIZE}\";/" *.rasi
grep -E font: *.rasi
