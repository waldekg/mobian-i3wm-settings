#!/usr/bin/bash

USERNAME=$1
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

sed -i 's/https:\/\/gitlab.com\//git@gitlab.com:/' $DIR/.git/config
echo "script is located at: $DIR"

function move_and_chown () {
	SRCFILE=$1
	DSTDIR=$2
	FILENAME=$3
	USER=$4
	echo "---"
	echo "source: $SRCFILE"
	echo "destination: $DSTDIR"
	echo "user: $4"
	rm -r "$DSTDIR/$FILENAME" && echo "removed $DSTDIR/$FILENAME"
	if [ ! -d "$DSTDIR" ]; then
		su - $USERNAME -c "mkdir -p $DSTDIR" && echo "created $DSTDIR"
	fi
	cp -r $SRCFILE "$DSTDIR/$FILENAME" && echo "moved $FILENAME to $DSTDIR"
	chown -R $USERNAME:$USERNAME "$DSTDIR/$FILENAME" && echo "chowned $FILENAME to $USERNAME"
}

# config file moving

SRCFILE="$DIR/settings/user/xresources.config" 
DSTDIR="/home/$USERNAME"
FILENAME=".Xresources"

move_and_chown $SRCFILE $DSTDIR $FILENAME $USERNAME

SRCFILE="$DIR/settings/user/i3.config" 
DSTDIR="/home/$USERNAME/.config/i3"
FILENAME="config"

move_and_chown $SRCFILE $DSTDIR $FILENAME $USERNAME

SRCFILE="$DIR/settings/user/onboard.config" 
DSTDIR="/home/$USERNAME/.config/onboard"
FILENAME="onboard-defaults.conf"

move_and_chown $SRCFILE $DSTDIR $FILENAME $USERNAME

SRCFILE="$DIR/settings/user/touchegg.config" 
DSTDIR="/home/$USERNAME/.config/touchegg"
FILENAME="touchegg.conf"

move_and_chown $SRCFILE $DSTDIR $FILENAME $USERNAME

SRCFILE="$DIR/polybarpine" 
DSTDIR="/home/$USERNAME/.config"
FILENAME="polybarpine"

move_and_chown $SRCFILE $DSTDIR $FILENAME $USERNAME

SRCFILE="$DIR/helper_scripts" 
DSTDIR="/home/$USERNAME/.local"
FILENAME="bin"

move_and_chown $SRCFILE $DSTDIR $FILENAME $USERNAME

# MPD stuff

MUSICFOLDER="/var/lib/mpd/music/$USERNAME"
if [ ! -d "$MUSICFOLDER" ]; then
	mkdir -p $MUSICFOLDER
	chown -R $USERNAME:audio $MUSICFOLDER
	ln -s $MUSICFOLDER /home/$USERNAME/music
fi
adduser mpd audio

SRCFILE="$DIR/settings/system/mpd.pa" 
DSTDIR="/etc/pulse"
FILENAME="mpd.pa"

move_and_chown $SRCFILE $DSTDIR $FILENAME root

SRCFILE="$DIR/settings/system/99-mpd.conf" 
DSTDIR="/etc/pulse/daemon.conf.d"
FILENAME="99-mpd.conf"

move_and_chown $SRCFILE $DSTDIR $FILENAME root

MPDEVIL=$(wget --quiet -O - https://api.github.com/repos/SoongNoonien/mpdevil/releases/latest | grep browser_ | grep deb | cut -d "\"" -f4)
wget $MPDEVIL -O /home/$USERNAME/mpdevil.deb
apt-get -y -qq install /home/$USERNAME/mpdevil.deb

# polybar/rofi stuff

## set all rofi fonts to match the polybar theme  

bash $DIR/polybarpine/forest/scripts/rofi/set_font.sh

# zsh and oh-my-zsh installation and configuration

OHMYZSHFOLDER="/home/$USERNAME/.oh-my-zsh"
if [ ! -d "$OHMYZSHFOLDER" ]; then
	su - $USERNAME -c "git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh"
	su - $USERNAME -c "cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc"
	sed -i 's/bin\/bash/usr\/bin\/zsh/g' /etc/passwd
fi

# system configuration

adduser $USERNAME users

## install touchegg

apt-get -y -qq install /home/$USERNAME/touchegg_2.0.4_arm64.deb

## lightdm configuration

cp -a $DIR/settings/system/lightdm.conf /etc/lightdm
cp -a $DIR/settings/system/lightdm-gtk-greeter.conf /etc/lightdm

## enable suspend as user

POLKITFOLDER="/var/lib/polkit-1/localauthority/50-local.d/"
if [ ! -d "$POLKITFOLDER" ]; then
	mkdir -p $POLKITFOLDER
fi
cp -a $DIR/settings/system/50-enable-suspend-on-lockscreen.pkla /var/lib/polkit-1/localauthority/50-local.d/
